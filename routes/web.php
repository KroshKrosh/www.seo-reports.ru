<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/artisan/{command}', function ($command) {
    return \Illuminate\Support\Facades\Artisan::call($command);
});

Route::get("/test","SiteController@test");

Route::get("/oauth/auth","YandexAuthController@auth");
Route::get("/oauth/start","YandexAuthController@start")->name('oauth.start');;

Route::get('/', function () {
    return redirect(url("/sites"));
});

Route::get("/sites/report/{id}","SiteController@report")->name("site.report");
Route::get("/sites/metrika","SiteController@metrika")->name("site.metrika");
Route::resource('sites', 'SiteController');

Route::group(["prefix" => "report"], function() {
    Route::get("html/{idSite}","ReportController@html")->name("report.html");
    Route::get("word/{idSite}","ReportController@word")->name("report.word");
    Route::get("pdf/{idSite}","ReportController@pdf")->name("report.pdf");
    Route::get("changeDate","ReportController@change_date")->name("report.changeDatePeriod");
});

Route::group(["prefix" => "history"], function() {
    Route::get("download/{id}","ReportHistoryController@download")->name("history.download");
    Route::get("delete/{id}","ReportHistoryController@delete")->name("history.delete");
});

Route::group(["prefix" => "reportPart"], function() {
    Route::post("save","ReportPartController@save")->name("reportPart.save");
    Route::get("params","ReportPartController@default_params")->name("reportPart.params");
    Route::get("{id}","ReportPartController@update")->name("reportPart.update");
});

Route::group(["prefix" => "template"], function() {
    Route::post("save","TemplateController@save");
    Route::get("load","TemplateController@load");
});
Route::resource('template', 'TemplateController');

Route::post('requests/import', 'SearchRequestController@import')->name('requests.import');
Route::resource('requests', 'SearchRequestController');

Route::resource('regions', 'RegionController');
