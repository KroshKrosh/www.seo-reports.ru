<ul class="nav nav-pills nav-stacked">
    @foreach($histories as $history)
        <li class = "active" data-history-id = "{!! $history->id !!}">
            <a href="#" onclick="return false" class="history">
                <div class="box-modal_close box-modal_close__white" onclick="deleteHistory({!! $history->id !!}); return false">X</div>
                <div>
                    <span onclick="downloadHistory( {!! $history->id !!})" >
                        @if($history->type == \App\Models\ReportHistory::$TYPE_WORD)
                            <i class="fa fa-2x fa-file-word-o" aria-hidden="true"></i>
                        @endif
                        @if($history->type == \App\Models\ReportHistory::$TYPE_PDF)
                            <i class="fa fa-2x fa-file-pdf-o" aria-hidden="true"></i>
                        @endif
                        @if($history->type == \App\Models\ReportHistory::$TYPE_HTML)
                            <i class="fa fa-2x fa-file-code-o" aria-hidden="true"></i>
                        @endif
                    </span>
                </div>
                <div class = "history_dates">
                    <div>{!! $history->dateStart !!} - {!! $history->dateEnd !!}</div>
                </div>
            </a>
        </li>
    @endforeach
</ul>
<script>
    function deleteHistory(id) {
        aja()
            .method('get')
            .url(window.Laravel.basePath + "/history/delete/"+id.toString())
            .on('200', (e) => {
                $("[data-history-id="+id.toString()+"]").slideUp();
            })
            .go()
    }

    function downloadHistory(id) {
        window.location = window.Laravel.basePath + "/history/download/"+id.toString();
    }
</script>
