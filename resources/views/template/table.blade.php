<table class="table table-responsive" id="template-table">
    <thead>
        <th style="width: 20%">Name</th>
        <th>Шаблон</th>
        <th style="width: 10%">Action</th>
    </thead>
    <tbody>
    @foreach($templates as $template)
        <tr>
            <td>{!! $template->name !!}</td>
            <td>
                <select data-id = "{!! $template->id !!}" multiple data-placeholder = "Содержимое шаблона..." class="js-chosen" style="width: 100%">
                    @foreach( $reportParts as $part)
                        <option @if($template->hasPart($part)) selected @endif value = {!! $part->getId() !!}>{!! $part->name !!}</option>
                    @endforeach
                </select>
            </td>
            <td>
                {!! Form::open(['route' => ['template.destroy', $template->id], 'method' => 'delete']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $(".js-chosen").chosen({disable_search_threshold: 10}).change(function(a,b,c) {
            saveChange(this);
        });
    });

    function saveChange(select)
    {
        let id = $(select).data("id");
        let value = $(select).val().join(",");
        aja()
            .method('post')
            .url(window.Laravel.basePath+"/template/save")
            .data({_token: window.Laravel.csrfToken, id: id, content: value})
            .on('200', (e) => {
                onSaveChange();
            })
            .go()
    }

    function onSaveChange() {

    }

</script>
