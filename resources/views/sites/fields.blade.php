<!-- Name Field -->
<div class="form-group col-sm-7">
    {!! Form::label('name', 'Наименование проекта:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Yandex Counter Field -->
<div class="form-group col-sm-7">
    {!! Form::label('yandex_counter', '№ счетчика Yandex:') !!}
    {!! Form::text('yandex_counter', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-7">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('sites.index') !!}" class="btn btn-default">Cancel</a>
</div>
