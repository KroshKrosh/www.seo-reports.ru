@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 style="margin-bottom: 0px;">{{ $site->name }}</h1>
            <h5 style="margin-top: 3px;">Отчет по сайту</h5>
        </div>
    </div>

    @include('adminlte-templates::common.errors')

    @include('flash::message')

    <div class = "col-md-9" style = "padding-left: 0px !important">

        {!! Form::open(['url' => 'foo/bar', 'id' => 'reportConfigForm', "class" => "form-inline"]) !!}
        @include('config.report')
        {!! Form::close() !!}

        <div class="btn-toolbar" role="toolbar">
            <div class="btn-group" role="group">

                <div class="dropdown">
                    <button style = "margin-bottom: 20px;" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Добавить элемент
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        @foreach ($reportParts as $part)
                            <li><a href="#" onclick="addToReport({!! $part->getId() !!}); return false;">{!! $part->name !!}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="btn-group" role="group">
                <button class="btn btn-success js-show-save-template-modal">
                    Сохранить как шаблон
                </button>
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Загрузить шаблон
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @foreach ($templates as $template)
                            <li><a href="#" onclick="loadTemplate({!! $template->id !!}); return false;">{!! $template->name !!}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="btn-group pull-right" role="group">
                <button type="button" class="btn btn-default js-report-convertToWord">Word</button>
                <button type="button" class="btn btn-default js-report-convertToPdf">PDF</button>
                <button type="button" class="btn btn-default js-report-convertToHtml">HTML</button>
            </div>
        </div>

        <riot-report>
        </riot-report>

        {{--
            Модалка "Сохранение шаблона"
        --}}
        <div style="display: none;">
            <div class="box-modal js-save-template-modal">
                <div class="box-modal_close arcticmodal-close">X</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Сохранение шаблона</h3>
                    </div>
                    <div class="panel-body box-modal_content">
                        <div class = "row">
                            <div class = "form-group">
                                <label>Имя шаблона</label>
                                <input class = "form-control js-template-name" name = "template-name" type="text" value="">
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-success js-save-template">
                                Сохранить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--
            Модалка "Настройки части шаблона"
        --}}
        <div style="display: none;">
            <div class="box-modal js-reportPart-config-modal">
                <div class="box-modal_close arcticmodal-close">X</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title js-reportPart-config-title"></h3>
                    </div>
                    <div class="panel-body box-modal_content js-reportPart-config-content">
                    </div>
                </div>
            </div>
        </div>

        <script>
            var report = null;
            var idSite = {!! $site->id !!}
            riot.compile(() => {
                var tags = riot.mount("riot-report", {
                    idSite: idSite,
                    paramsUrl: "{!! route("reportPart.params") !!}",
                    loadTemplateUrl: "{!! url("/template/load") !!}",
                    configUrl: "{!! url("/reportPart") !!}",
                    configCallback: loadConfig,
                    saveTemplateUrl: "{!! url("/template/save") !!}"
                });
                report = tags[0];
            });

            function addToReport(id) {
                report.addById(id);
            }

            function loadTemplate(id) {
                report.loadTemplate(id, onLoadTemplate);
            }

            function saveTemplate(name) {
                report.saveTemplate(name, onSaveTemplate);
            }

            function onLoadTemplate() {
                var n = noty({text: 'Шаблон загружен', type: 'success', timeout: 5000});
            }

            function onSaveTemplate() {
                $.arcticmodal('close');
                var n = noty({text: 'Шаблон сохранен', type: 'success', timeout: 5000});
            }

            function loadConfig(data) {
                $('.js-reportPart-config-title').text(data.title);
                $('.js-reportPart-config-content').html(data.content);
                $('.js-reportPart-config-form').submit(function(e) {
                    aja()
                        .method('post')
                        .url(this.action)
                        .data(formToObj(this))
                        .on('200', (e) => {
                            onSaveConfig();
                        })
                        .go()
                    e.preventDefault();
                    return false;
                })
                $('.js-reportPart-config-modal').arcticmodal();
            }

            function onSaveConfig()
            {
                $.arcticmodal('close');
                var n = noty({text: 'Настройки блока сохранены', type: 'success', timeout: 5000});
            }

            // Служебная функция для конвертации формы в json объект
            function formToObj(form) {
                var properties = $(form).serializeArray();
                var result = {};
                properties.forEach((elem) => {
                    if (typeof result[elem.name] !== "undefined") {
                        // Свойство уже было записано, значит нужно записывать в массив

                        // Если здесь не массив, то надо его создать
                        if (!Array.isArray(result[elem.name]))
                        {
                            let temp = result[elem.name];
                            result[elem.name] = [];
                            result[elem.name].push(temp);
                        }
                        result[elem.name].push(elem.value);
                    } else {
                        result[elem.name] = elem.value;
                    }
                });
                return result;
            }


        </script>

        <script>
            $(document).ready( function() {
                function printReport(url, e) {
                    window.location.href = url+"?params="+encodeURIComponent(report.getReportConfig());
                    e.preventDefault();
                }

                $(".js-report-convertToHtml").click(function(e) {
                    printReport("<?=route("report.html",["id" => $site->id]) ?>", e);
                });

                $(".js-report-convertToWord").click(function(e) {
                    printReport("<?=route("report.word",["id" => $site->id]) ?>", e);
                });

                $(".js-report-convertToPdf").click(function(e) {
                    printReport("<?=route("report.pdf",["id" => $site->id]) ?>", e);
                });

                $(".js-show-save-template-modal").click(function(e) {
                    $('.js-save-template-modal').arcticmodal();
                });

                $(".js-save-template").click(function(e) {
                    saveTemplate($('.js-template-name').val());
                });

            });
        </script>
    </div>



    <div class="col-md-3">
       @include('report_history.index')
    </div>



@endsection
