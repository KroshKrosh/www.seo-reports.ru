{!! Form::open(['route' => ['requests.import'], 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'js-import-form']) !!}

<div class="form-group col-sm-7">
    {!! Form::label('date', 'Дата съемки позиций:') !!}
    {!! Form::date('date',null,['class' => 'form-control js-import-date', "required" => true])!!}
    {!! Form::label('file', 'Файл с позициями:') !!}
    {!! Form::file('file', ['class' => 'form-control js-import-file', "required" => true]) !!}
    {!! Form::hidden('idSite', $site->id) !!}
    {!! Form::hidden('_token', csrf_token()) !!}
</div>
<div class="form-group col-sm-7">
        {!! Form::submit('Импортировать',["class" => "btn btn-success"]) !!}
</div>

<script>
    $(document).ready(function() {
  /*      $(".js-import-file").change(function() {
            var formData= new FormData();
            formData.append('idSite',{!! $site->id; !!});
        formData.append('date',$(".js-import-date").val());
        formData.append('_token',"{!! csrf_token(); !!}");
        var file = $(this)[0].files[0];
        formData.append('file',file);
        $.ajax({
            type: 'POST',
            processData: false, // important
            contentType: false, // important
            url: '{!! route("requests.import")!!}',
            type : 'POST',
            datatype: 'text',
            data : formData,
            processData: false,
            success: function(data)
            {
                requestGridView.syncItems();
            },
            error: function()
            {
            }
        });
        return false;

    })*/
    });
</script>

{!! Form::close() !!}
