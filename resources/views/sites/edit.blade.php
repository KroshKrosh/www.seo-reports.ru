@extends('layouts.app')

@section('content')

        @include('flash::message')

        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Редактирование сайта</h1>
            </div>
        </div>

        @include('adminlte-templates::common.errors')

<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Основная информация</a></li>
        <li role="presentation"><a href="#requests" aria-controls="requests" role="tab" data-toggle="tab">Запросы и позиции</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="main">
            <div class="row">
                {!! Form::model($site, ['route' => ['sites.update', $site->id], 'method' => 'patch']) !!}

                @include('sites.fields')

                {!! Form::close() !!}
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="requests">

            <div>
                <h1>Импортировать позиции:</h1>
                <h3>Последние позиции загружены на {!! date("d.m.Y", strtotime($site->dateLastImport)) !!}</h3>
                @include('sites.import')
            </div>


            <div class="col-sm-12">
                <h1>Импортированные запросы:</h1>
                <riot-gridview>
                </riot-gridview>
            </div>
            <script>
                var requestGridView;
                riot.compile(function() {
                    var tags = riot.mount("riot-gridview", {url: "<?=url("/requests") ?>", requestInfo: {"_token": "<?=csrf_token();?>", "idSite":<?=$site->id; ?>}});
                    requestGridView = tags[0];
                    requestGridView.syncItems();
                });
            </script>

        </div>
    </div>

</div>
@endsection
