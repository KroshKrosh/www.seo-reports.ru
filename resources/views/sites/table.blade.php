<table class="table table-responsive" id="sites-table">
    <thead>
        <th>Name</th>
        <th>Yandex Counter</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($sites as $site)
        <tr>
            <td>{!! $site->name !!}</td>
            <td>{!! $site->yandex_counter !!}</td>
            <td>
                {!! Form::open(['route' => ['sites.destroy', $site->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('site.report', [$site->id]) !!}" class='btn btn-default btn-xs'>Отчет</a>
                    <a href="{!! route('sites.edit', [$site->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
