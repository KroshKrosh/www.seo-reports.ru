
<div class="form-group" style = "margin-bottom: 10px;">
    {!! Form::label("dateRange", "Период формирования отчета: ") !!}
    {!! Form::text("dateRange", $config->dateOrDefault(), ['class' => 'form-control']) !!}
</div>
<div style="display:none" id="nonFullInfo">
    За выбранный период отсутствуют данные метрики. <a href = "{!! route("oauth.start",["backUrl" => Request::url(), "idSite" => $site->id]) !!}">Нажмите на ссылку, чтобы загрузить данные</a>
</div>


<script>
    $(document).ready( function() {
        $('input[name="dateRange"]').daterangepicker(
            {
                locale: {
                    format: 'YYYY-MM-DD'
                },
                minDate: '{{ $config->getMinDate()}}',
                maxDate: '{{ $config->getMaxDate() }}'
            },
            function(dateStart, dateEnd) {
                //Почему-то dateStart выдается на день раньше, чем выбран пользователем
                //Следующий код это фиксит
                var realDateStart = new Date(dateStart);
                realDateStart.setDate(realDateStart.getDate() + 1);

                aja()
                    .method('get')
                    .url('<?=route("report.changeDatePeriod") ?>')
                    .data({dateRange: realDateStart.toISOString().substring(0, 10) + " - "+dateEnd.toISOString().substring(0, 10), idSite: {!! $site->id !!}})
                    .on('200', function(response){
                        if (response.result == "non-full")
                        {
                            $("#nonFullInfo").slideDown();
                        } else {
                            $("#nonFullInfo").slideUp();
                            report.updateTemplate();
                        }
                    })
                    .go();
            });
        $(".js-reportPart_content_toggle").change(function(elem) {
            if ($(this).is(":checked"))
                $($($(this).parent()).parent()).find(".js-reportPart_content").slideDown();
            else
                $($($(this).parent()).parent()).find(".js-reportPart_content").slideUp();
        }).change();
    })
</script>