<?php
    use App\Models\ReportPartConfig;
?>

    {!! Form::open(["route" => ["reportPart.save"], "class" => "js-reportPart-config-form"]) !!}

    {!! Form::hidden("id",$reportPartConfig->idReportPart) !!}


    {!! Form::hidden("fields[]",ReportPartConfig::$PARAM_TEXT_IS_LOWER) !!}
    {!! Form::hidden("fields[]",ReportPartConfig::$PARAM_TEXT_IS_HIGHER) !!}
    {!! Form::hidden("fields[]",ReportPartConfig::$PARAM_TEXT_IS_EQUAL) !!}
    {!! Form::hidden("fields[]",ReportPartConfig::$PARAM_EPSILON) !!}
    {!! Form::hidden("fields[]",ReportPartConfig::$PARAM_METRIC_CODE) !!}


    {!! Form::hidden(ReportPartConfig::$PARAM_METRIC_CODE,$reportPartConfig->reportPart->getMetricCode()) !!}

    <div class="form-group">
        {!! Form::label(ReportPartConfig::$PARAM_TEXT_IS_LOWER,"Текст при уменьшении метрики") !!}
        {!! Form::textarea(ReportPartConfig::$PARAM_TEXT_IS_LOWER,$reportPartConfig->getParam(ReportPartConfig::$PARAM_TEXT_IS_LOWER),["class" => "form-control"]) !!}
    </div>

    <div class="form-group">
        {!! Form::label(ReportPartConfig::$PARAM_TEXT_IS_HIGHER,"Текст при увеличении метрики") !!}
        {!! Form::textarea(ReportPartConfig::$PARAM_TEXT_IS_HIGHER,$reportPartConfig->getParam(ReportPartConfig::$PARAM_TEXT_IS_HIGHER),["class" => "form-control"]) !!}
    </div>

    <div class="form-group">
        {!! Form::label(ReportPartConfig::$PARAM_TEXT_IS_EQUAL,"Текст при неизменной метрики") !!}
        {!! Form::textarea(ReportPartConfig::$PARAM_TEXT_IS_EQUAL,$reportPartConfig->getParam(ReportPartConfig::$PARAM_TEXT_IS_EQUAL),["class" => "form-control"]) !!}
    </div>

    <div class="form-group">
        {!! Form::label(ReportPartConfig::$PARAM_EPSILON,"Порог для изменения метрики") !!}
        {!! Form::text(ReportPartConfig::$PARAM_EPSILON,$reportPartConfig->getParam(ReportPartConfig::$PARAM_EPSILON),["class" => "form-control"]) !!}
    </div>


{!! Form::submit("Сохранить",["class" => "btn btn-success js-btn-save-config"]) !!}


{!! Form::close() !!}
