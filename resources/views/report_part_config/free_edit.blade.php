<?php
    use App\Models\ReportPartConfig;
?>

    {!! Form::open(["route" => ["reportPart.save"], "class" => "js-reportPart-config-form"]) !!}

    {!! Form::hidden("id",$reportPartConfig->idReportPart) !!}

    {!! Form::hidden("fields[]",ReportPartConfig::$PARAM_FIELD_CONTENT) !!}

    <div class="form-group">
        <riot-summernote id="config-summernote">

        </riot-summernote>
    </div>

    <textarea id="config-content" name = "{!! ReportPartConfig::$PARAM_FIELD_CONTENT !!}" style="display: none">

    </textarea>


{!! Form::submit("Сохранить",["class" => "btn btn-primary js-btn-save-config"]) !!}

{!! Form::close() !!}
    <script>
        var tags = riot.mount("#config-summernote", {
            defaultValue: `{!! $reportPartConfig->getParam(ReportPartConfig::$PARAM_FIELD_CONTENT)  !!}`
        });
        tags[0].$editor.on('summernote.change', (we, e) => {
            $("#config-content").val(e);
        })

    </script>
