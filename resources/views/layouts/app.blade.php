<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SuperSeo') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    --}}<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{URL::asset('/css/app.css')}}" rel="stylesheet">

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>


    <script src="{{URL::asset('/chosen/chosen.jquery.min.js')}}"></script>
    <link href="{{URL::asset('/chosen/chosen.css')}}" rel="stylesheet">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.11/packaged/jquery.noty.packaged.min.js"></script>

    <!-- arcticModal -->
    <script src="{{URL::asset('/arcticmodal/jquery.arcticmodal.js')}}"></script>
    <link rel="stylesheet" href="{{URL::asset('/arcticmodal/jquery.arcticmodal.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/arcticmodal/themes/simple.css')}}">

    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <script src="{{URL::asset('js/components/riot-report.tag.html')}}" type="riot/tag"></script>
    <script src="{{URL::asset('js/components/riot-report-tags.tag.html')}}" type="riot/tag"></script>
    <script src="{{URL::asset('js/components/riot-summernote.tag.html')}}" type="riot/tag"></script>
    <script src="{{URL::asset('js/components/riot-gridview-row.tag.html')}}" type="riot/tag"></script>
    <script src="{{URL::asset('js/components/riot-gridview.tag.html')}}" type="riot/tag"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aja/0.4.1/aja.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/riot/3.0.7/riot+compiler.min.js"></script>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'basePath' => url("/"),
        ]); ?>
    </script>



</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/sites') }}">
                        {{ config('app.name', 'SuperSeo') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                         <li><a href="{{ route('sites.index') }}">Список сайтов</a></li>
                         <li><a href="{{ route('template.index') }}">Шаблоны отчетов</a></li>

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('breadcrumbs')

        <div class="col-md-10 col-md-offset-1">
            @yield('content')
        </div>
    </div>

    <!-- Scripts -->


</body>
</html>
