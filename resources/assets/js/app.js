$(document).ready(() => {
    $('input[name="dateRange"]').daterangepicker(
        {
            locale: {
                format: 'YYYY-MM-DD'
            },
            startDate: '2013-01-01',
            endDate: '2013-12-31'
        },
})