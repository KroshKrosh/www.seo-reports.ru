<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchRequestPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_request_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("idRequest");
            $table->integer("idRegion");
            $table->integer("position");
            $table->integer("idSearchEngine");
            $table->date("dateMeasure");
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_request_positions');
    }
}
