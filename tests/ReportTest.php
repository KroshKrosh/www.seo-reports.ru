<?php

use App\Helpers\WordHelper;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

class ReportTest extends TestCase
{
    public function testFreeText()
    {
        $phpWord = new PhpWord();
        $section = $phpWord->addSection();
        $text = '<p><img src="https://vk.com/images/stickers/94/128.png" style="width: 128px;"><br></p>';

        $config = array(
            'indent'         => true,
            'output-xhtml'   => true,
            'wrap'           => 200);

        $tidy = new \tidy();
        $tidy->parseString($text,$config);
        $tidy->cleanRepair();
        $text = $tidy->body();
      //  $this->assertTrue(false,$text);

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $text);

        \PhpOffice\PhpWord\Settings::setPdfRendererPath(base_path('/vendor/dompdf/dompdf'));
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

        $objWriter = IOFactory::createWriter($phpWord, 'PDF');
        $filename = 'test.pdf';
        $objWriter->save($filename);

    }
}
