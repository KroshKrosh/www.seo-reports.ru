<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TableTest extends TestCase
{
    public function getStub()
    {
        $site = new \App\Models\Site();
        $repository = new \App\Repositories\ReportPartRepository($site);
        $reportConfig = new \App\Models\ReportConfig();
        $stub = $this->getMockForAbstractClass('\App\Models\ReportParts\Table',[$repository, $reportConfig]);
        return $stub;
    }

    public function getPrivateMethod($name)
    {
        $class = new \ReflectionClass('\App\Models\ReportParts\Table');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }


    /**
     * Проверка геттера и сеттера maxColumnsInRow
     */
    public function testMaxColumnsInRow()
    {
        $stub = $this->getStub();
        $stub->setMaxColumnsInRow(2);
        $this->assertEquals($stub->getMaxColumnsInRow(), 2);
    }

    /**
     * Проверка на корректное разделение таблицы
     */
    public function testStripTables()
    {
        $method = $this->getPrivateMethod("getStrippedTables");
        $stub = $this->getStub();

        $stub->setMaxColumnsInRow(2);
        $stub->setData([[["value" => "Test"],["value" => 1],["value" => 1]],[["value" => 1],["value" => 1],["value" => 1]],[["value" => 1],["value" => 1],["value" => 1]]]);
        $result = $method->invokeArgs($stub,[]);
        // Проверка, что таблицы разделены
        $this->assertTrue(isset($result[0][0][0]) && isset($result[1][0][0]));
        // Проверка, что шапка продублирована
        $this->assertTrue($result[1][0][0]["value"] == "Test",json_encode($result[1][0][0]));

        $stub->setMaxColumnsInRow(5);
        $result = $method->invokeArgs($stub,[]);
        // Проверка, что таблицы не разделены
        $this->assertTrue(isset($result[0][0][0]) && !isset($result[1][0][0]),json_encode($result));

    }
}
