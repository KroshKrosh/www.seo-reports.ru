<?php

namespace App\Models;

use App\Repositories\ReportPartRepository;
use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Exception;

/**
 * App\Models\ReportPartConfig
 *
 * @property int $id
 * @property int $idReportPart
 * @property string $config
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportPartConfig whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportPartConfig whereIdReportPart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportPartConfig whereConfig($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportPartConfig whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportPartConfig whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReportPartConfig extends Model
{
    /**
     * @var ReportPart
     */
    public $reportPart = null;

    public static $PARAM_TEXT_IS_LOWER = "text_is_lower";
    public static $PARAM_TEXT_IS_HIGHER = "text_is_upper";
    public static $PARAM_TEXT_IS_EQUAL = "text_is_equal";
    public static $PARAM_EPSILON = "epsilon";
    public static $PARAM_METRIC_CODE = "metric_code";

    public static $PARAM_FIELD_CONTENT = "field_content";

    public function loadReportPart($idReportPart)
    {
        $this->idReportPart = $idReportPart;
        $parts = (new ReportPartRepository(new Site()))->getAll();
        if (!isset($parts[$idReportPart]))
            throw new \Exception("Не найдена часть отчета");
        $this->reportPart = $parts[$idReportPart];
    }


    protected $params = [];

    public function loadFromParams(array $params)
    {
        $this->idReportPart = $params["id"];
        $fields = explode(",",$params["fields"][0]);
        foreach ($fields as $field)
        {
            $this->params[$field] = $params[$field];
        }
    }


    public function getParams()
    {
        return $this->params;
    }

    public function getEditForm()
    {
        return $this->reportPart->getConfigView();
    }

    private $delimeter = "~!~";

    public function serialize()
    {
        $result = "";
        foreach ($this->params as $key => $value)
        {
            if ($result != "")
                $result.= $this->delimeter;
            $result.= $key.$this->delimeter.$value;
        }
        $this->config = $result;
    }

    public function unserialize()
    {
        $mas = explode($this->delimeter, $this->config);
        if (count($mas) < 2)
            return;
        for ($i = 0; $i<count($mas); $i += 2)
        {
            $this->params[$mas[$i]] = $mas[$i+1];
        }
    }


    public function getParam($name)
    {
        if (!isset($this->params[$name]))
            return "";
//             throw new Exception("Не найдено свойство");
        return $this->params[$name];
    }

    public function getTitle()
    {
        return "Настройка блока \"".$this->reportPart->name."\"";
    }

}
