<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\SearchRequest
 *
 * @property int $id
 * @property string $name
 * @property int $idSite
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequest whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequest whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequest whereIdSite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequest whereDeletedAt($value)
 * @mixin \Eloquent
 */
class SearchRequest extends Model
{
    use SoftDeletes;

    public $table = 'search_requests';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'idSite'
    ];

    public $fieldSearchable = [
        'name',
        'idSite'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'idSite' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function getPosition($dateMeasure,$idRegion,$idSearchEngine)
    {
        $pos = SearchRequestPosition::where("idRequest",$this->id)
            ->where("idRegion",$idRegion)
            ->where("idSearchEngine",$idSearchEngine)
            ->where("dateMeasure","<",$dateMeasure)
            ->orderBy("dateMeasure", "desc")
            ->first();
        if ($pos != null)
            return $pos->position;
        return -1;
    }

}
