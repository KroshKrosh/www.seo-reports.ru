<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\MetricHelper;
use App\Models\ReportConfig;
use App\Models\ReportPart;
use App\Models\ReportPartConfig;
use Exception;
use PhpOffice\PhpWord\PhpWord;

class DimensionDiagram extends Diagram
{
    private $_metricCode = null;

    /**
     * @param $metricCode
     * @throws \Exception Бросает ошибку, если указан неправильный код метрики
     */
    public function setMetricCode($metricCode)
    {
        if (array_search($metricCode,MetricHelper::getYandexMetricCodes()) === false)
            throw new \Exception("Неправильный код метрики");

        $this->_metricCode = $metricCode;
        $this->_chartName = MetricHelper::getYandexMetricNames()[$this->_metricCode];
        $this->name = MetricHelper::getYandexMetricNames()[$this->_metricCode];
    }

    private $deltaPercent = 0;
    private $deltaAbsolute = 0;

    public function getMetricCode()
    {
        return $this->_metricCode;
    }

    /**
     * Вспомогательная функция, прореживает массив, оставляя в нем указанное кол-во элементов
     * Например, thinKeys([1,2,3,4,5,6],3) вернет [1,4,6]
     * @param $array Массив для прореживания
     * @param int $needElem Желаемое кол-во элементов в итоговом массиве
     * @return array Прореженный массив
     */
    private function thinKeys($array, $needElem = 5)
    {
        if (count($array) == 0)
            return [];
        $result = [];
        $needNum = $needElem - 1;
        $count = count($array);
        for ($i = 0; $i <= $needNum; $i++)
        {
            $index = min(floor($count * $i / $needNum),$count-1);
            $result[] = $array[$index];
        }
        return $result;
    }



    public function updateConfig(ReportConfig $config)
    {
        parent::updateConfig($config);
        $this->updateData();
        $this->useConfig();
    }


    /**
     * Применяет настройки к текущим данным
     */
    public function useConfig()
    {
        $this->_config->getReportPartConfig($this)->unserialize();
        $this->title = "Диаграмма изменения метрики \"{$this->name}\"";

        $startValue = $this->_chartValues[0];
        $endValue = $this->_chartValues[count($this->_chartValues) - 1];
        $delta = abs($endValue - $startValue);
        $deltaPercent = $delta * 100 / max($startValue,1);
        if ($deltaPercent >= +$this->_config->getReportPartConfig($this)->getParam(ReportPartConfig::$PARAM_EPSILON))
        {
            if ($endValue > $startValue)
                $this->commentary = $this->_config->getReportPartConfig($this)->getParam(ReportPartConfig::$PARAM_TEXT_IS_HIGHER);
            else
                $this->commentary = $this->_config->getReportPartConfig($this)->getParam(ReportPartConfig::$PARAM_TEXT_IS_LOWER);
        } else
        {
            $this->commentary = $this->_config->getReportPartConfig($this)->getParam(ReportPartConfig::$PARAM_TEXT_IS_EQUAL);
        }
        $this->deltaPercent = $deltaPercent * (($endValue > $startValue) ? 1 : -1);
        $this->deltaAbsolute = $delta;
    }


     public function getCalcedParams()
     {
         return ["title" => $this->title, "commentary" => $this->commentary, "imageUrl" => $this->getChartUrl(), "name" => $this->name, "tag" =>"dimension-diagram", "metric" => $this->_metricCode, "id" => $this->getId(), "deltaPercent" => $this->deltaPercent, "deltaAbsolute" => $this->deltaAbsolute];
     }

     public function getConfigView()
     {
         return "dimension";
     }

    /**
     * Получение значений метрики
     * @throws Exception Выбрасывается если прежде не вызывался setMetricCode
     */
    public function updateData()
    {
        $data = [];
        if ($this->_metricCode == null)
            throw new Exception("Не указано измерение для диаграммы");

        $keys = [];

        foreach ($this->_config->getDatePeriod() as $day)
        {
            $metric = $this->_site->getMetricValue($this->_metricCode,$day->format("Y-m-d"));
            $data[] = $metric;
            $keys[] = $day->format("d.m.Y");
        }

        $keys = $this->thinKeys($keys);

        $this->setChartData("","line",$keys,$data);
    }

    public function addToPhpWord(PhpWord $wordFile)
    {
        $this->updateData();
        return parent::addToPhpWord($wordFile);
    }

    public function addToHtml()
    {
        $this->updateData();
        return parent::addToHtml();
    }

}