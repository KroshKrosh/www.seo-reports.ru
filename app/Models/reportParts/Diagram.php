<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\ChartHelper;
use App\Models\ReportPart;
use PhpOffice\PhpWord\PhpWord;

abstract class Diagram extends ReportPart
{
    protected $_chartKeys;
    protected $_chartValues;
    protected $_chartName;
    protected $_chartType;


    public function setChartData($name,$type,array $keys,array $values)
    {
        $this->_chartKeys = $keys;
        $this->_chartValues = $values;
        $this->_chartName = $name;
        $this->_chartType = $type;
    }

    public function getChartUrl()
    {
        return ChartHelper::getChart($this->_chartType,$this->_chartValues,$this->_chartKeys);
    }

    public function addToPhpWord(PhpWord $wordFile)
    {
        $section = $wordFile->addSection(['breakType' => 'continuous']);
        $section->addTitle($this->title, 2);
        $section->addImage($this->getChartUrl());
        $section->addText($this->commentary);
        return parent::addToPhpWord($wordFile);
    }

    public function addToHtml()
    {
        return view("report_parts.diagram",["data" => $this]);
    }

}