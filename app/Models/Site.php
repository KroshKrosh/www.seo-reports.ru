<?php

namespace App\Models;

use App\Helpers\MetricHelper;
use App\Models\Dimension;
use DateTime;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yandex\Common\Exception\ForbiddenException;
use Yandex\Metrica\Stat\AvailableValues;
use Yandex\Metrica\Stat\Models\TableParams;
use Yandex\Metrica\Stat\StatClient;

/**
 * App\Models\Site
 *
 * @property int $id
 * @property string $name
 * @property string $yandex_counter
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $dateLastImport
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereYandexCounter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Site whereDateLastImport($value)
 * @mixin \Eloquent
 */
class Site extends Model
{
    use SoftDeletes;

    public $table = 'sites';

    public static function getYandexMetrics()
    {
        return ["ym:s:visits" => "Посещения",
            "ym:s:users" => "Посетители",
            "ym:s:hits" => "Хиты",
            "ym:s:newUsers" => "Новые посетители" ,
            "ym:s:avgVisitDurationSeconds" => "Средняя продолжительность визита",
            "ym:s:pageDepth" => "Глубина просмотра"];
    }



    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'yandex_counter'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'yandex_counter' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];


    /**
     * Проверяет наличие полных данных о метрике за период в БД
     * @param string $metricCode Код метрики в формате яндекса
     * @param DateTime $dateStart Дата начала периода
     * @param DateTime $dateEnd Дата окончания периода
     * @return bool Возвращает истину если в БД полные данные
     */
    public function hasFullMetricInfo($metricCode, DateTime $dateStart, DateTime $dateEnd)
    {
        $dbCount = DB::table("dimensions")
            ->where("idSite", $this->id)
            ->where("yandex_metricCode", $metricCode)
            ->where("dateMeasure", ">=", $dateStart->format("Y-m-d"))
            ->where("dateMeasure", "<=", $dateEnd->format("Y-m-d"))
            ->groupBy("dateMeasure")
            ->select("dateMeasure")
            ->get()->toArray();
        $dbCount = count($dbCount);
//        var_dump($dbCount);
        $diff = $dateEnd->diff($dateStart)->days + 1;
//        var_dump($diff);
        return $dbCount >= $diff;
    }


    /**
     * Проверяет, есть ли данные Метрики за указанный период в БД
     * Если нет, то загружает их
     * @param DateTime $dateStart Дата начала периода
     * @param DateTime $dateEnd Дата конца периода (не включается)
     * @param $token Токен для доступа
     * @throws ForbiddenException Выбрасывается в случае, если у авторизованного пользователя нет прав к этой Метрике
     */
    public function prepareMetrics(DateTime $dateStart, DateTime $dateEnd, $token)
    {
        $tempMetricCode = "ym:s:visits";
        if (!$this->hasFullMetricInfo($tempMetricCode, $dateStart, $dateEnd))
        {
            $this->clearMetrics($dateStart,$dateEnd);
            $interval = \DateInterval::createFromDateString("1 day");
            $dateEnd->modify("+1 day");
            $period = new \DatePeriod($dateStart,$interval,$dateEnd);
            foreach ($period as $day)
            {
                $this->loadStatistic($day->format("Y-m-d"), $token);
            }
        }
    }

    /**
     * Удаляет из базы значения метрики
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     */
    private function clearMetrics(DateTime $dateStart, DateTime $dateEnd)
    {
        DB::table("dimensions")->where("idSite",$this->id)
            ->where("dateMeasure",">=",$dateStart->format("Y-m-d"))
            ->where("dateMeasure","<=",$dateEnd->format("Y-m-d"))
            ->delete();
    }


    /**
     * Загружает данные из Метрики за выбранную дату
     * @param string $date Строка с датой в формате Y-m-d
     * @param string $token Токен для доступа
     */
    public function loadStatistic($date, $token)
    {
        Dimension::where("dateMeasure",$date)
            ->where("idSite",$this->id)
            ->delete();

        $codeMetrics = array_keys(MetricHelper::getYandexMetricNames());
        $date = str_replace("-","",$date);

        $statClient = new StatClient($token);

        $paramsModel = new TableParams();
        $paramsModel->setPreset(AvailableValues::PRESET_SOURCES_SUMMARY)
            ->setMetrics(implode(",",$codeMetrics))
            ->setDate1($date)
            ->setDate2($date)
            ->setId($this->yandex_counter);

        $data = $statClient->data()->getTable($paramsModel);

        if ($data) {
            if (!is_null($data->getData())) {
                foreach ($data->getData() as $dimensions) {
                    $dims = $dimensions->getDimensions()->getAll();
                    $name1 = $dims[0]->getName();
                    $name2 = $dims[1]->getName();

                    for ($i = 0; $i < count($data->getQuery()->getMetrics()); $i++) {
                        $val = $dimensions->getMetrics()[$i];
                        $dimension = new Dimension();
                        $dimension->idSite = $this->id;
                        $dimension->yandex_dimension1Name = $name1;
                        $dimension->yandex_dimension2Name = $name2;
                        $dimension->dateMeasure = $date;
                        $dimension->yandex_metricCode = $codeMetrics[$i];
                        $dimension->value = $val;
                        $dimension->save();
                    }
                }
            }
        }
    }


    /**
     * Возвращает статистику посещаемости по источникам
     * Используется для круговой диаграммы
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @return array Имя_источника => значение
     */
    public function getSourcesStatisic(DateTime $dateStart, DateTime $dateEnd)
    {
        $metricCode = "ym:s:visits";
        $query = DB::table("dimensions")
            ->where("idSite",$this->id)
            ->where("dateMeasure",">=",$dateStart->format("Y-m-d"))
            ->where("dateMeasure","<=",$dateEnd->format("Y-m-d"))
            ->where("yandex_metricCode",$metricCode)
            ->groupBy("yandex_dimension1Name")
            ->select("yandex_dimension1Name",DB::raw('SUM(value) as total_value'))
            ->get();
        $result = [];
        foreach ($query as $item)
        {
            $result[$item->yandex_dimension1Name] = $item->total_value;
        }
        return $result;
    }

    /**
     * @return SearchRequest[]
     */
    public function getSearchRequests()
    {
        return SearchRequest::where("idSite",$this->id)
            ->get();
    }

    /**
     * Возвращает значение указанной метрики на дату
     * @param $metricCode
     * @param $dateMeasure
     * @return mixed
     */
    public function getMetricValue($metricCode, $dateMeasure)
    {
        $metric = \App\Models\Dimension::where("idSite",$this->id)
            ->where("yandex_metricCode",$metricCode)
            ->where("dateMeasure","=",$dateMeasure)
//            ->orderBy("dateMeasure","desc")
            ->sum("value");
        return $metric;
    }

    // todo:: remake this function
    /**
     * @return Region[]
     */
    public function getRegions()
    {
        $ids = DB::table('regions')
            ->join("search_request_positions","regions.id","=","search_request_positions.idRegion")
            ->join("search_requests","search_requests.id","=","search_request_positions.idRequest")
            ->join("sites","sites.id","=","search_requests.idSite")
            ->where("sites.id",$this->id)
            ->pluck("regions.id");
        return Region::whereIn("id",$ids)->get();
    }

    /**
     * Получает позицию по запросу
     * @param Region $region
     * @param SearchRequest $request
     * @param $dateMeasure
     * @return int|mixed Возвращается позиция или -1, если позиция бне была снята
     */
    public function getRequestPosition(Region $region, SearchRequest $request, $dateMeasure)
    {
        return $request->getPosition($dateMeasure,$region->id,1);
    }


    /**
     * @param Region $region
     * @param $dateMeasure
     * @return array Таблица с элементами [request, position]
     */
    public function getAllRequestsPositionsTable(Region $region, $dateMeasure)
    {
        $requests = $this->getSearchRequests();
        $result = [];
        foreach ($requests as $item)
        {
            $result[] = [
                "request" => $item,
                "position" => $this->getRequestPosition($region, $item, $dateMeasure),
            ];
        }
        return $result;
    }

    /**
     * Возвращает таблицу с сравнением позиций по dateMeasure1 и dateMeasure2
     * @param Region $region Регион, по которому проводится поиск
     * @param string $dateMeasure1 Наименьшая дата
     * @param string $dateMeasure2 наибольшая дата для сравнения
     * @return array
     */
    public function getAllRequestsPositionsCompareTable(Region $region, $dateMeasure1, $dateMeasure2)
    {
        $result = $this->getAllRequestsPositionsTable($region, $dateMeasure1);
        foreach ($result as $key => $row)
        {
            $result[$key]["newPosition"] = $this->getRequestPosition($region, $row["request"], $dateMeasure2);
        }
        return $result;
    }

}
