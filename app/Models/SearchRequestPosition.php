<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SearchRequestPosition
 *
 * @property int $id
 * @property int $idRequest
 * @property int $idRegion
 * @property int $position
 * @property int $idSearchEngine
 * @property string $dateMeasure
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereIdRequest($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereIdRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereIdSearchEngine($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereDateMeasure($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SearchRequestPosition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SearchRequestPosition extends Model
{
    //
}
