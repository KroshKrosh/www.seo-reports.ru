<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;


use App\Repositories\ReportPartRepository;
use PhpOffice\PhpWord\PhpWord;

abstract class ReportPart {

    public $title = "";
    public $commentary = "";


    protected $_site;

    private $_id = 0;

    public function getId()
    {
        return $this->_id;
    }

    public $name = "Report part";

    /**
     * @var ReportConfig
     */
    protected $_config;

    public function getConfigPart()
    {
        return $this->_config->getReportPartConfig($this);
    }

    public function __construct(ReportPartRepository $repository, ReportConfig $config)
    {
        $this->_site = $repository->getSite();
        $this->_id = $repository->getNextPartId();
        $this->_config = $config;
    }

    public function updateConfig(ReportConfig $config)
    {
        $this->_config = $config;
    }

    public function applyParams(array $params)
    {
        foreach ($params as $key => $value)
        {
            if (isset($this->$key))
                $this->$key = $value;
        }
    }


    public function addToHtml()
    {
        return "dadasdas";
    }

    public function addToPhpWord(PhpWord $wordFile)
    {

    }

    /**
     * Функция возвращает массив с рассчитаными начальными значениями для объекта
     * Должен возвращать значения по ключам name, tag, title, id
     * Остальное - в зависимости от шаблона
     * @return array
     */
    public abstract function getCalcedParams();

    /**
     * Функция возвращает имя view для редактирования настроек
     * @return string
     */
    public abstract function getConfigView();
}