<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 07.12.16
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models;



use DateInterval;
use DateTime;

class ReportConfig {

    protected $_dateStart = null;
    protected $_dateEnd = null;

    /**
     * Возвращает массив с первым и последним выбранным днем
     * @return array
     */
    public function getPeriod()
    {
        return [$this->_dateStart, $this->_dateEnd];
    }

    public function setPeriod($dateStart, $dateEnd)
    {
        $this->_dateStart = new \DateTime($dateStart);
        $this->_dateEnd = new DateTime($dateEnd);
        // Костыль для включения последнего дня в диапазоне
    //    $this->_dateEnd = $this->_dateEnd->modify( '+1 day' );
    }

    public function setPeriodFromString($param)
    {
        $dates = explode(" - ",$param);
        $this->setPeriod($dates[0],$dates[1]);
    }

    protected $_interval;

    public function getInterval()
    {
        return $this->_interval;
    }

    public function setInterval($interval)
    {
        $this->_interval = DateInterval::createFromDateString($interval);
    }



    protected $_datePeriod = null;

    /**
     * Возвращает объект типа DatePeriod, представляющий выбранный диапазон дат с установленным интервалом
     * Используется для перебора в периодических расчетах
     * @return \DatePeriod
     */
    public function getDatePeriod()
    {
        if ($this->_datePeriod == null)
        {
            $this->_datePeriod = new \DatePeriod($this->_dateStart,$this->_interval,$this->_dateEnd);
        }
        return $this->_datePeriod;
    }

    public function clearDatePeriod()
    {
        $this->_datePeriod = null;
    }

    public function getMaxDate()
    {
        $cur = new DateTime();
        $cur->modify("-1 day");
        return $cur->format("Y-m-d");
    }

    public function getMinDate()
    {
        $cur = new DateTime("2016-10-01");
        return $cur->format("Y-m-d");
    }

    public function dateOrDefault()
    {
        if ($this->_dateStart == null)
            return $this->getMinDate()." - ".$this->getMaxDate();
        return implode(" - ",array_map(function($elem) {
            return $elem->format("Y-m-d");
        },$this->getPeriod()));
    }


    public function __construct()
    {
        $defaultInterval = "1 day";
        $this->setInterval($defaultInterval);
    }

    public function setFromRequest(\Illuminate\Http\Request $request)
    {
        $defaultDateRange = $request->session()->get("dateRange",date("Y-m-d",time() - 3600 * 24 * 30)." - ".date("Y-m-d",time(3600 * 24)));
        list($dateStart, $dateEnd) = explode(" - ",$defaultDateRange);
        $this->setPeriod($dateStart, $dateEnd);
    }



    private $reportPartConfigs = [];


    /**
     * @param ReportPart $part
     * @return ReportPartConfig
     */
    public function getReportPartConfig(ReportPart $part)
    {
        if (!isset($this->reportPartConfigs[$part->getId()]))
        {
            $model = ReportPartConfig::where("idReportPart",$part->getId())
                ->first();
            if ($model == null)
            {
                $model = new ReportPartConfig();
            }
            $model->unserialize();
            $this->reportPartConfigs[$part->getId()] = $model;
        }
        return $this->reportPartConfigs[$part->getId()];
    }


}