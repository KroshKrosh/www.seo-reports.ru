<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Dimension
 *
 * @property int $id
 * @property int $idSite
 * @property string $dateMeasure
 * @property string $yandex_metricCode
 * @property string $yandex_dimension1Name
 * @property string $yandex_dimension2Name
 * @property float $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereIdSite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereDateMeasure($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereYandexMetricCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereYandexDimension1Name($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereYandexDimension2Name($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Dimension whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Dimension extends Model
{
    //
}
