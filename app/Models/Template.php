<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Template
 *
 * @property int $id
 * @property string $name
 * @property int $idUser
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template whereIdUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $content
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template whereContent($value)
 */
class Template extends Model
{
    public function hasPart(ReportPart $part)
    {
        $arr = explode(",",$this->content);
        return array_search($part->getId(), $arr) !== false;
    }
}
