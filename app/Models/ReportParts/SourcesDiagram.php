<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\MetricHelper;
use App\Models\ReportConfig;
use App\Models\ReportPart;
use Exception;
use PhpOffice\PhpWord\PhpWord;

class SourcesDiagram extends Diagram
{

    public $name = "Источники трафика";

    public function updateConfig(ReportConfig $config)
    {
        parent::updateConfig($config);
        $this->updateData();
    }

    public function updateData()
    {
        $data = $this->_site->getSourcesStatisic($this->_config->getPeriod()[0], $this->_config->getPeriod()[1]);
        $keys = array_keys($data);
//        $keys = array_map(function ($elem) {return urlencode($elem);}, $keys );

        $this->setChartData("","pie",$keys,$data);
    }

    public function getConfigView()
    {
        return "no_config";
    }


    public function addToPhpWord(PhpWord $wordFile)
    {
        $this->updateData();
        return parent::addToPhpWord($wordFile);
    }

    public function addToHtml()
    {
        $this->updateData();
        return parent::addToHtml();
    }

    public function getCalcedParams()
    {
        return ["title" => $this->title, "commentary" => $this->commentary, "imageUrl" => $this->getChartUrl(), "name" => $this->name, "tag" =>"sources-diagram", "id" => $this->getId()];
    }

}