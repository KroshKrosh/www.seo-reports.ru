<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\WordHelper;
use App\Models\ReportPart;
use App\Models\ReportPartConfig;
use PhpOffice\PhpWord\PhpWord;

class FreeHtmlText extends ReportPart
{
    public $content = "";
    public $name = "Свободный ввод";

    public function prepareContent()
    {
        $config = array(
            'indent'         => true,
            'output-xhtml'   => true,
            'wrap'           => 200);

        $tidy = new \tidy();
        $tidy->parseString($this->content,$config,"utf8");
        $tidy->cleanRepair();

        $text = $tidy->body();
        $text = str_replace('<body>','',$text);
        $text = str_replace('</body>','',$text);

        $this->content = $text;
    }

    public function addToPhpWord(PhpWord $wordFile)
    {
        $this->prepareContent();
        $section = $wordFile->addSection(['breakType' => 'continuous']);
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $this->content, false, ["Heading1" => WordHelper::$centerParagraphStyle, "Heading1Font" => WordHelper::$h1FontStyle]);
    }

    public function getCalcedParams()
    {
        $fieldContent = $this->getConfigPart()->getParam(ReportPartConfig::$PARAM_FIELD_CONTENT);
        return ["title" => $this->name, "name" => $this->name, "tag" =>"free-text", "id" => $this->getId(), "field_content" => $fieldContent];
    }

    public function getConfigView()
    {
        return "free_edit";
    }

}