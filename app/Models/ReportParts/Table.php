<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\ChartHelper;
use App\Helpers\WordHelper;
use App\Models\ReportPart;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\PhpWord;

abstract class Table extends ReportPart
{
    private $_data;

    /**
     * Если столбцов в таблице больше этого значения, то она будет разбиваться на несколько подтаблиц
     * @var int
     */
    private $maxColumnsInRow = 5;

    /**
     * @return int
     */
    public function getMaxColumnsInRow()
    {
        return $this->maxColumnsInRow;
    }

    /**
     * @param int $maxColumnsInRow
     */
    public function setMaxColumnsInRow($maxColumnsInRow)
    {
        $this->maxColumnsInRow = $maxColumnsInRow;
    }


    public $name = "Table";



    public function setData($data)
    {
        $this->_data = $data;
    }

    public function getData()
    {
        return $this->_data;
    }

    /**
     * Возвращает массив с $this->_data, разделенной на таблицы, в каждой из которых не более $this->maxColumnsInRow
     * @see $this->maxColumnsInRow
     * @return array
     */
    private function getStrippedTables()
    {
        $columnCount = count($this->_data[0]);
        $result = [];
        $curStrippedTable = -1;
        $totalWrited = 0;
        while ($totalWrited < $columnCount)
        {
            $curStrippedTable++;
            $result[] = [];
            foreach ($this->_data as $row)
            {
                $result[$curStrippedTable][] = [];
            }
            $curColumnInStrippedTable = 0;
            // Если это не первая табличка, то нужно повторно вставить первый столбец
            if ($curStrippedTable > 0)
            {
                $rowNum = 0;
                foreach ($this->_data as $row)
                {
                    $result[$curStrippedTable][$rowNum++][0] = $row[0];
                }
                $curColumnInStrippedTable++;
            }
            while ($curColumnInStrippedTable < $this->maxColumnsInRow && $totalWrited < $columnCount)
            {
                $rowNum = 0;
                foreach ($this->_data as $row)
                {
                    $result[$curStrippedTable][$rowNum++][$curColumnInStrippedTable] = $row[$totalWrited];
                }
                $totalWrited++;
                $curColumnInStrippedTable++;
            }
        }
        return $result;
    }

    private function addTable(Section $section)
    {
        $strippedTables = $this->getStrippedTables();
        foreach ($strippedTables as $strippedTable)
        {
            $table = $section->addTable();
            foreach ($strippedTable as $row)
            {
                $table->addRow();
                foreach ($row as $column)
                {
                    $val = $column["value"];
                    $style = $column["style"];
                    $width = $column["width"];
                    $font = $column["font"];
                    $params = array_merge($column["params"], WordHelper::$noSpaceParagraphStyle);
                    $table->addCell($width,$style)->addText($val,$font,$params);
                }
            }
        }
    }

    public function addToPhpWord(PhpWord $wordFile)
    {
        $section = $wordFile->addSection(['breakType' => 'continuous']);
        $section->addTitle($this->title, 2);

        $this->addTable($section);

        $section->addText($this->commentary);
        return parent::addToPhpWord($wordFile);
    }

    public function addToHtml()
    {
        return view("report_parts.table",["data" => $this]);
    }

    public function applyParams(array $params)
    {
        $this->setMaxColumnsInRow($params["column_count"]);
        parent::applyParams($params);
    }


}