<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\ChartHelper;
use App\Helpers\WordHelper;
use App\Models\ReportPart;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\PhpWord;

class RequestTable extends Table
{

    public $name = "Таблица позиций по запросам";

    public function updateData()
    {
        $dateMeasure1 = $this->_config->getMinDate();
        $dateMeasure2 = $this->_config->getMaxDate();
        $data = [];
        $regions = $this->_site->getRegions();
        foreach ($regions as $region)
        {
            $row = [];
            $row["region"]  = $region;
            $row["data"] = $this->_site->getAllRequestsPositionsCompareTable($region,$dateMeasure1,$dateMeasure2);
            $data[] = $row;
        }
        $this->setData($this->setFromRequestData($data));
    }


    private function setFromRequestData($data)
    {
        $style_header = [];
        $style_top10 = ["bgColor" => "3CFF00"];
        $style_top30 = ["bgColor" => "FFC400"];
        $default_width = 1700;

        $headerFontStyle = WordHelper::$boldFontStyle;
        $tableFontStyle = [];

        $table = [];
        // header row
        $header = [];
        $header[] = ["style" => $style_header, "value" => "", "width" => $default_width, "font" => $headerFontStyle, "params" => WordHelper::$centerParagraphStyle];
        foreach ($data as $row)
        {
            $header[] = ["value" => $row["region"]->name, "style" => $style_header, "width" => $default_width, "font" => $headerFontStyle, "params" => WordHelper::$centerParagraphStyle];
        }
        $table[] = $header;

        // body
        foreach ($data[0]["data"] as $key => $item)
        {
            $request = $item["request"];
            $newRow = [];
            $newRow[] = ["style" => [], "value" => $request->name, "width" => 1700, "font" => $headerFontStyle, "params" => WordHelper::$centerParagraphStyle];
            foreach ($data as $row)
            {
                $style = [];
                $position = $row["data"][$key]["position"];
                $newPosition = $row["data"][$key]["newPosition"];
                if ($newPosition <= 30)
                    $style = $style_top30;
                if ($newPosition <= 10)
                    $style = $style_top10;
                if ($newPosition <= 10 && ($position > 10 || $position == 0))
                    $newPosition.=" (new)";

                $newRow[] = ["value" => $newPosition, "style" => $style, "width" => $default_width, "font" => $tableFontStyle, "params" => WordHelper::$centerParagraphStyle];
            }
            $table[] = $newRow;
        }
        return $table;
    }

    public function addToPhpWord(PhpWord $wordFile)
    {
        $this->updateData();
        return parent::addToPhpWord($wordFile);
    }

    public function getConfigView()
    {
        return "request_table";
    }


    public function addToHtml()
    {
        $this->updateData();
        return parent::addToHtml();
    }

    public function getCalcedParams()
    {
        return ["title" => $this->title, "commentary" => $this->commentary, "name" => $this->name, "tag" =>"request-table", "column_count" => $this->getMaxColumnsInRow(), "id" => $this->getId()];
    }

}