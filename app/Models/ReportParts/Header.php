<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 06.12.16
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */

namespace App\Models\ReportParts;


use App\Helpers\WordHelper;
use App\Models\ReportPart;
use App\Models\ReportPartConfig;
use PhpOffice\PhpWord\PhpWord;

class Header extends ReportPart
{
    public $name = "Шапка";

    public function addToPhpWord(PhpWord $wordFile)
    {
        $section = $wordFile->addSection(["marginTop" => -300]);
      //  $header = $section->addHeader();

        // TODO:: move to view template
        $image = $section->addImage(public_path("btb_logo.png"), [
            'width'            => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(5),
            'height'           => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(3),
            'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE,
            'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
            'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
            'posVertical'      => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
            'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_LINE,
        ]);

        $section->addText('тел. +7 (3852) 25-07-85, +7 962791-44-55');
        $section->addText('www.btb.su	e-mail: nk@btb.su');
        $section->addText('656039, Россия, Алтайский край, г. Барнаул');
        $section->addText('ул. Энтузиастов, д.52 оф. 206а');

        $section->addText("Отчет по продвижению сайта", WordHelper::$h2FontStyle, WordHelper::$centerParagraphStyle);

        $section->addText($this->_site->name, WordHelper::$h1FontStyle, WordHelper::$centerParagraphStyle);

        $table = $section->addTable([]);
        $row = $table->addRow();
        $cell = $row->addCell(20000);
        $cell->addText($this->getCity());
        $cell = $row->addCell(8000);
        $cell->addText($this->getDate(),[],WordHelper::$rightParagraphStyle);

    }

    public function getDate()
    {
        return "27 января 2017 г.";
    }

    public function getCity()
    {
        return "г. Барнаул";
    }

    public function getCalcedParams()
    {
//        $fieldContent = $this->getConfigPart()->getParam(ReportPartConfig::$PARAM_FIELD_CONTENT);
        return ["title" => $this->name, "name" => $this->name, "tag" =>"header", "id" => $this->getId()];
    }

    public function getConfigView()
    {
        return "no_config";
    }

}