<?php

namespace App\Models;

use Dompdf\Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ReportHistory
 *
 * @property int $id
 * @property int $idUser
 * @property int $idTemplate
 * @property int $idSite
 * @property int $type
 * @property string $fileName
 * @property string $dateStart
 * @property string $dateEnd
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereIdUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereIdTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereIdSite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereDateEnd($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReportHistory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReportHistory extends Model
{
    public static $TYPE_WORD = 1;
    public static $TYPE_PDF = 2;
    public static $TYPE_HTML = 3;

    public function getTypeAsString() {
        if ($this->type == self::$TYPE_WORD)
            return "docx";
        if ($this->type == self::$TYPE_PDF)
            return "pdf";
        if ($this->type == self::$TYPE_HTML)
            return "html";
        throw new Exception("Неизвестный type");
    }

    public function getPath()
    {
        $site = Site::find($this->idSite);
        $dirPath = config('app.mediaPath');
        return $dirPath.$site->name."_".$this->dateStart."-".$this->dateEnd."_".$this->id.'.'.$this->getTypeAsString();

    }

    public function getDownloadPath()
    {
        return url("history.download")."/".$this->id;
    }

    public function write()
    {
        $filename = $this->getPath();
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($filename));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        flush();
        readfile($filename);
    }

    public function deleteFile()
    {
        unlink($this->getPath());
    }

}
