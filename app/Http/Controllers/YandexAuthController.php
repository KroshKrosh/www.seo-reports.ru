<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yandex\Metrica\Management\ManagementClient;
use Yandex\OAuth\Exception\AuthRequestException;
use Yandex\OAuth\OAuthClient;

class YandexAuthController extends Controller
{
    /**
     * @var string ID приложения
     */
    public $id = "ec39d9e34d484342a3a56b598eacaddb";

    /**
     * @var string Secret приложения
     */
    public $secret = "97ab7556905a4d61851dffe8dafb1bec";


    /**
     * Запрос oauth авторизации
     * @param Request $request
     */
    public function start(Request $request)
    {
        $backUrl = $request->get("backUrl");
        $dateRange = $request->session()->get("dateRange");
        $idSite = $request->get("idSite");
        $params = serialize(["backUrl" => $backUrl, "dateRange" => $dateRange, "idSite" => $idSite]);
        $oauth = new OAuthClient($this->id);
        $oauth->authRedirect(true,OAuthClient::CODE_AUTH_TYPE,$params);
    }


    /**
     * Обработка oauth авторизации
     * После чего происходит редирект на метод для получения значений метрики
     * @param Request $request
     * @return mixed
     */
    public function auth(Request $request)
    {
        $client = new OAuthClient($this->id, $this->secret);
        $code = $request->get("code");
        $state = $request->get("state");
        try {
            // осуществляем обмен
            $client->requestAccessToken($code);
        } catch (AuthRequestException $ex) {
            echo $ex->getMessage();
        }

        $token = $client->getAccessToken();
        $request->session()->put("token",$token);
        $params = unserialize($state);

        return Redirect::route("site.metrika",$params);
    }
}
