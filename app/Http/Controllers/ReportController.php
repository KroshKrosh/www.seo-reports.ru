<?php

namespace App\Http\Controllers;

use App\Helpers\MetricHelper;
use App\Helpers\WordHelper;
use App\Models\ReportConfig;
use App\Models\ReportHistory;
use App\Models\Site;
use App\Repositories\ReportHistoryRepository;
use App\Repositories\ReportPartRepository;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\PhpWord;

class ReportController extends Controller
{

    /**
     * TODO:: убрать отсюда???
     * @param $phpWord
     */
    private function addDefaultStyles($phpWord)
    {
        $phpWord->setDefaultFontName("DejaVu Sans");
        $phpWord->addFontStyle("Heading1", WordHelper::$h1FontStyle);
        $phpWord->addFontStyle("Heading2", WordHelper::$h2FontStyle);

    }
    /**
     * Печатает части отчета в PhpWord
     * TODO:: может, стоит это перенести в другой класс????
     * @param Request $request
     * @param Site $site
     * @return PhpWord
     */
    private function generate(request $request, Site $site)
    {
        $phpWord = new PhpWord();
        $this->addDefaultStyles($phpWord);
        $partsRepository = new ReportPartRepository($site);
        $config = new ReportConfig();
        $config->setFromRequest($request);

        $params = $request->get("params");
        $params = json_decode($params);
        foreach ($params as $item)
        {
            $elements = explode("&",$item);
            $params = [];
            foreach ($elements as $element)
            {
                list($key, $value) = explode("=",$element);
                $value = urldecode($value);
                $params[$key] = $value;
            }
            $part = $partsRepository->getByParams($params, $config);
            if ($part != null)
                $part->addToPhpWord($phpWord);
        }

        return $phpWord;
    }


    /**
     * Подготавливает данные к печати
     * @param Request $request
     * @param $idSite
     * @return array
     */
    private function prepare(Request $request, $idSite, $type)
    {
        $site = Site::findOrFail($idSite);
        $phpWord = $this->generate($request, $site);
        $history = ReportHistoryRepository::add($request,$site,$type);
        $name = $history->fileName;
        return [$phpWord, $name];
    }

    public function html(Request $request, $idSite)
    {
        list($phpWord, $name) = $this->prepare($request, $idSite, ReportHistory::$TYPE_HTML);
        return WordHelper::saveAsHtml($phpWord, $name);
    }

    public function word(Request $request, $idSite)
    {
        list($phpWord, $name) = $this->prepare($request, $idSite, ReportHistory::$TYPE_WORD);
        return WordHelper::saveAsWord($phpWord, $name);
    }

    public function pdf(Request $request, $idSite)
    {
        list($phpWord, $name) = $this->prepare($request, $idSite, ReportHistory::$TYPE_PDF);
        return WordHelper::saveAsPdf($phpWord, $name);
    }

   public function change_date(Request $request)
    {
        $metricCode = MetricHelper::getYandexMetricCodes()[0];
        $site = Site::findOrFail($request->get("idSite"));
        $config = new ReportConfig();
        $request->session()->set("dateRange",$request->get("dateRange"));
        $config->setPeriodFromString($request->get("dateRange"));
        if ($site->hasFullMetricInfo($metricCode, $config->getPeriod()[0], $config->getPeriod()[1]))
        {
            return ["result" => "full"];
        } else
        {
            return ["result" => "non-full"];
        }
    }

}
