<?php

namespace App\Http\Controllers;

use App\Helpers\MetricHelper;
use App\Helpers\WordHelper;
use App\Models\ReportConfig;
use App\Models\ReportPartConfig;
use App\Models\Site;
use App\Repositories\ReportPartRepository;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

/**
 * На самом деле это контроллер для ReportPartConfig
 * @package App\Http\Controllers
 */
class ReportPartController extends Controller
{

    public function save(Request $request)
    {
        $id = $request->get("id");
        $model = ReportPartConfig::where("idReportPart",$id)
            ->first();
        if ($model == null)
            $model = new ReportPartConfig();
        $model->loadFromParams($request->all());
        $model->serialize();
        $model->save();
    }

    public function update(Request $request, $id)
    {
        $model = ReportPartConfig::where("idReportPart",$id)
            ->first();
        if ($model == null)
            $model = new ReportPartConfig();
        $model->loadReportPart($id);
        $model->unserialize();
        return ["result" => "success", "title" => $model->getTitle(), "content" => view("report_part_config.".$model->getEditForm(),["reportPartConfig" => $model])->render()];
    }

    public function default_params(Request $request)
    {
        $idReportPart = $request->get("idReportPart");
        $idSite = $request->get("idSite");
        $site = Site::findOrFail($idSite);
        $config = new ReportConfig($site);
        $config->setFromRequest($request);

        $repository = new ReportPartRepository($site);
        $reportParts = $repository->getAll($config);
        foreach ($reportParts as $item)
        {
            if ($item->getId() == $idReportPart)
            {
                $item->updateConfig($config);
                return $item->getCalcedParams();
            }
        }

        throw new Exception("Не найдена часть отчета");
    }
}
