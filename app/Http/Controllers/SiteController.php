<?php

namespace App\Http\Controllers;

use App\Helpers\ChartHelper;
use App\Helpers\SeoHelper;
use App\Helpers\WordHelper;
use App\Models\ReportConfig;
use App\Models\ReportHistory;
use App\Repositories\ReportHistoryRepository;
use App\Repositories\ReportPartRepository;
use App\Repositories\TemplateRepository;
use DateTime;
use App\Http\Requests;
use App\Models\Site;
use App\Repositories\SiteRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Style\Font;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Yandex\Common\Exception\ForbiddenException;

class SiteController extends InfyOmBaseController
{
    /** @var  SiteRepository */
    private $siteRepository;

    public function __construct(SiteRepository $siteRepo)
    {
        $this->siteRepository = $siteRepo;
    }

    public function getToken(Request $request)
    {
        return $request->session()->get("token");
    }


    /**
     * Загружает данные из метрики
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function metrika(Request $request)
    {
        $site = Site::findOrFail($request->get("idSite"));
        $dateRange = explode(" - ",$request->get("dateRange"));
        $dateStart = new DateTime($dateRange[0]);
        $dateEnd = new DateTime($dateRange[1]);
        try
        {
            $site->prepareMetrics($dateStart, $dateEnd, $this->getToken($request));
        }
        catch (ForbiddenException $e)
        {
            Flash::error('У вас нет прав доступа к данной статистике');
            return redirect(url($request->get("backUrl","/sites/")));
        }

        Flash::success('Данные из Яндекс.Метрики успешно загружены');
        return redirect(url($request->get("backUrl","/sites/")));
    }

    /**
     * Display a listing of the Site.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->siteRepository->pushCriteria(new RequestCriteria($request));
        $sites = $this->siteRepository->all();

        return view('sites.index')
            ->with('sites', $sites);
    }


    /**
     * Форма отчета сайта
     * @param Request $request
     * @param $id Id Site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function report(Request $request, $id)
    {
        $site = Site::findOrFail($id);
        $config = new ReportConfig();
        $config->setFromRequest($request);

        $reportPartRepository = new ReportPartRepository($site);
        $reportParts = $reportPartRepository->getAll();
        foreach ($reportParts as $item)
        {
            $item->updateConfig($config);
        }

        $templates = TemplateRepository::get();
        $histories = ReportHistoryRepository::get(["idSite" => $site->id]);

        return view("sites.report",["site" => $site, "config" => $config, "reportParts" => $reportParts, "templates" => $templates, "histories" => $histories]);
    }




    /**
     * Show the form for creating a new Site.
     *
     * @return Response
     */
    public function create()
    {
        return view('sites.create');
    }

    /**
     * Store a newly created Site in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $site = $this->siteRepository->create($input);

        Flash::success('Site saved successfully.');

        return redirect(route('sites.index'));
    }

    /**
     * Display the specified Site.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $site = $this->siteRepository->findWithoutFail($id);


        if (empty($site)) {
            Flash::error('Site not found');

            return redirect(route('sites.index'));
        }

        return view('sites.show')->with('site', $site);
    }

    /**
     * Show the form for editing the specified Site.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $site = $this->siteRepository->findWithoutFail($id);

        if (empty($site)) {
            Flash::error('Site not found');

            return redirect(route('sites.index'));
        }

        return view('sites.edit')->with('site', $site);
    }

    /**
     * Update the specified Site in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $site = $this->siteRepository->findWithoutFail($id);

        if (empty($site)) {
            Flash::error('Site not found');

            return redirect(route('sites.index'));
        }

        $site = $this->siteRepository->update($request->all(), $id);

        Flash::success('Site updated successfully.');

        return redirect(route('sites.index'));
    }

    /**
     * Remove the specified Site from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $site = $this->siteRepository->findWithoutFail($id);

        if (empty($site)) {
            Flash::error('Site not found');

            return redirect(route('sites.index'));
        }

        $this->siteRepository->delete($id);

        Flash::success('Site deleted successfully.');

        return redirect(route('sites.index'));
    }
}
