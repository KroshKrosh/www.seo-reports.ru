<?php

namespace App\Http\Controllers;

use App\Models\ReportConfig;
use App\Models\Site;
use App\Models\Template;
use App\Models\ReportHistory;
use App\Repositories\ReportPartRepository;
use App\Repositories\TemplateRepository;
use Flash;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Prettus\Repository\Criteria\RequestCriteria;

class ReportHistoryController extends InfyOmBaseController
{
   public function delete($id)
   {
       $history = ReportHistory::findOrFail($id);
       // TODO:: ВЫполнять удаление файла автоматически в модели при вызове delete
       $history->deleteFile();
       $history->delete();

   }

   public function download($id)
   {
       $history = ReportHistory::findOrFail($id);
       $history->write();



   }


}
