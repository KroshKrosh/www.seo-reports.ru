<?php

namespace App\Http\Controllers;

use App\Models\ReportConfig;
use App\Models\Site;
use App\Models\Template;
use App\Repositories\ReportPartRepository;
use App\Repositories\TemplateRepository;
use Flash;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Prettus\Repository\Criteria\RequestCriteria;

class TemplateController extends InfyOmBaseController
{
    /** @var  TemplateRepository */
    private $templateRepository;

    /**
     * TemplateController constructor.
     * @param TemplateRepository $siteRepo
     */
    public function __construct(TemplateRepository $tempRepo)
    {
        $this->templateRepository = $tempRepo;
    }

    public function save(Request $request)
    {
        $id = $request->get("id");
        $name = $request->get("name");
        $content = $request->get("content");

        if ($id != null)
            $template = Template::find($id);
        else
            $template = Template::where("name",$name)
                // TODO:::: add user checking
                ->first();
        if ($template == null)
        {
            $template = new Template();
            //TODO:: save user info
            $template->name = $name;
        }
        //todo:: remove when add user support
        $template->idUser = 1;
        $template->content = is_null($content) ? "" : $content;
        $result = $template->save();
    }

    public function load(Request $request)
    {
        $id = $request->get("id");
        $template = Template::findOrFail($id);
        return $template;
    }

    public function index(Request $request)
    {
        $this->templateRepository->pushCriteria(new RequestCriteria($request));
        $templates = $this->templateRepository->all();

        $site = new Site();
        $config = new ReportConfig();
        $config->setFromRequest($request);

        $reportPartRepository = new ReportPartRepository($site);
        $reportParts = $reportPartRepository->getAll();

        return view('template.index')
            ->with('templates', $templates)
            ->with('reportParts', $reportParts);
    }

    public function destroy($id)
    {
        $template = $this->templateRepository->findWithoutFail($id);

        if (empty($template)) {
            Flash::error('Шаблон не найден');

            return redirect(route('template.index'));
        }

        $this->templateRepository->delete($id);

        Flash::success('Шаблон успешно удален');

        return redirect(route('template.index'));
    }




}
