<?php

namespace App\Http\Controllers;

use App\Helpers\ExcelHelper;
use App\Models\Region;
use App\Models\SearchRequest;
use App\Models\SearchRequestPosition;
use App\Models\Site;
use App\Repositories\SearchRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Laracasts\Flash\Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SearchRequestController
 * @package App\Http\Controllers
 */

class SearchRequestController extends InfyOmBaseController
{
    /** @var  SearchRequestRepository */
    private $searchRequestRepository;

    public function __construct(SearchRequestRepository $searchRequestRepo)
    {
        $this->searchRequestRepository = $searchRequestRepo;
    }

    public function import(Request $request)
    {
        $site = Site::findOrFail($request->get("idSite"));
        $date = $request->get("date");
        $data = ExcelHelper::readFromFile($request->file("file"));
        $headerRegionRow = 5;
        $regions = [];
        $searchEngines = [];
        for ($i = 1; $i < count($data[$headerRegionRow]); $i++)
        {
            $fullRegionName = trim($data[$headerRegionRow][$i]);
            $searchEngineText = explode(".",$fullRegionName)[0];
            // TODO:: доделать получение поисковой машины
            $searchEngines[] = 1;

            preg_match('/\((.+)\)/', $fullRegionName, $m);
            $regionName = $m[1];

            $region = Region::where("name",$regionName)
                ->first();
            if ($region == null)
            {
                $region = new Region();
                $region->name = $regionName;
                $region->code = 1;
                $region->save();
            }
            $regions[] = $region;
        }
        for ($row = $headerRegionRow+1; $row<count($data); $row++)
        {
            $requestText = trim($data[$row][0]);
            $searchRequest = SearchRequest::where("idSite",$site->id)
                ->where("name",$requestText)
                ->first();
            if ($searchRequest == null)
            {
                $searchRequest = new SearchRequest();
                $searchRequest->idSite = $site->id;
                $searchRequest->name = $requestText;
                $searchRequest->save();
            }
            for ($i = 0; $i<count($regions); $i++)
            {
                $value = (int)trim($data[$row][$i+1]);
                $pos = new SearchRequestPosition();
                $pos->dateMeasure = $date;
                $pos->idRequest = $searchRequest->id;
                $pos->idRegion = $regions[$i]->id;
                $pos->idSearchEngine = $searchEngines[$i];
                $pos->position = $value;
                $pos->save();
                // make search request position
            }
        }

        if ($site->dateLastImport == "" || strtotime($site->dateLastImport) < strtotime($date))
        {
            $site->dateLastImport = $date;
            $site->save();
        }

        Flash::success('Информация о позициях успешно загружена');

        return redirect(url("sites/".$site->id."/edit"));
    }


    public function index(Request $request)
    {
        $searchRequests = $this->searchRequestRepository->findByField("idSite",$request->get("idSite"));

        return $this->sendResponse($searchRequests->toArray(), 'SearchRequests retrieved successfully');
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $searchRequests = $this->searchRequestRepository->create($input);

        return $this->sendResponse($searchRequests->toArray(), 'SearchRequest saved successfully');
    }


    public function show($id)
    {
        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->searchRequestRepository->find($id);

        if (empty($searchRequest)) {
            return Response::json(ResponseUtil::makeError('SearchRequest not found'), 404);
        }

        return $this->sendResponse($searchRequest->toArray(), 'SearchRequest retrieved successfully');
    }


    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->searchRequestRepository->find($id);

        if (empty($searchRequest)) {
            return Response::json(ResponseUtil::makeError('SearchRequest not found'), 404);
        }

        $searchRequest = $this->searchRequestRepository->update($input, $id);

        return $this->sendResponse($searchRequest->toArray(), 'SearchRequest updated successfully');
    }


    public function destroy($id)
    {
        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->searchRequestRepository->find($id);

        if (empty($searchRequest)) {
            return Response::json(ResponseUtil::makeError('SearchRequest not found'), 404);
        }

        $searchRequest->delete();

        return $this->sendResponse($id, 'SearchRequest deleted successfully');
    }
}
