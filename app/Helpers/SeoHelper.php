<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 22.11.16
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */

namespace App\Helpers;


class SeoHelper {
    // Инициализация curl
    function curl_start($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_ENCODING,'gzip,deflate');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; ru:1");
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    // Получаем количество страниц в индексе Яндекса
    public function yandex_index($site_url)
    {
        $content = $this->curl_start('https://yandex.ru/search/?text=site%3A'.urlencode($site_url));
        $content = str_replace('&nbsp;тыс.','000',$content);
        $content = str_replace('&nbsp;млн','000000',$content);
        $content = str_replace('ничего не найдено','0',$content);
        var_dump($content);
//        preg_match('~Нашлось (\d) результатов~',$content,$match);
//        return $match[1];
    }
    // Получаем количество страниц в индексе Google
    public function google_index($site_url)
    {
        return "";
        $result_string = file_get_contents("http://www.google.ru/search?q=site%3a" . $site_url);
        // TODO:: Дикий костыль, нужно сделать как-то по-другому
        if (strpos($result_string, "ничего не найдено") !== false) {
            return 0;
        } else {
            var_dump($result_string);
            return;
            $match = preg_match("/about ([0-9,]{0,12})/i", $result_string, $matches);
            echo $matches[1];
        }

     }
}
