<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 08.12.16
 * Time: 17:53
 * To change this template use File | Settings | File Templates.
 */

namespace App\Helpers;


class MetricHelper {

    /**
     * Возвращает массив, в котором ключи - коды метрик, а значения - человекопонятные названия
     * @return array
     */
    public static function getYandexMetricNames()
    {
        return ["ym:s:visits" => "Посещения",
            "ym:s:users" => "Посетители",
            "ym:s:hits" => "Просмотры",
            "ym:s:newUsers" => "Новые посетители" ,
            "ym:s:avgVisitDurationSeconds" => "Средняя продолжительность визита",
            "ym:s:pageDepth" => "Глубина просмотра"];
    }

    /**
     * Возвращает массив с кодами метрик
     * @return array
     */
    public static function getYandexMetricCodes()
    {
        return array_keys(self::getYandexMetricNames());
    }



}