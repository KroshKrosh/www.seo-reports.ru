<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 22.11.16
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

namespace App\Helpers;


class ChartHelper {

    public static function getChart($type, $series, $labels)
    {
        $chartType = "";
        if ($type == "line")
            $chartType = "lc";
        if ($type == "pie")
            $chartType = "p";
        $size = "650x200";
        $data = implode(",",$series);
        $categories =implode("|", array_map(function($elem) {
            return urlencode($elem);
        },$labels));
        return "https://chart.googleapis.com/chart?chds=a&chs={$size}&chd=t:{$data}&cht={$chartType}&chl={$categories}&chxt=x,y&chco=2F7ED8|8BBC22|0D233A|00A5C6|1AADCE|F28F43";
    }

}