<?php

namespace App\Helpers;

/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 16.11.16
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
use PHPExcel_IOFactory;

/**
 * Class ExcelHelper
 * @package App\Helpers
 */
class ExcelHelper {

    /**
     * Возвращает массив с построчно считанным файлом Excel
     * @param $file Экземпляр файла (например, $request->file())
     * @return array
     */
    public static function readFromFile($file){
        // TODO:: выбрасывать ошибки
        $filePath = $file->getPathname();
        $inputFileType = PHPExcel_IOFactory::identify($filePath);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($filePath);

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $result = [];
        for ($row = 1; $row <= $highestRow; $row++){
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                NULL,
                TRUE,
                FALSE);
            $result[] = $rowData[0];
        }
        return $result;
    }

}