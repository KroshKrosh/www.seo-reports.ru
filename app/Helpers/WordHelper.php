<?php

namespace App\Helpers;

/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 16.11.16
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

/**
 * Class WordHelper
 * @package App\Helpers
 */
class WordHelper {

    public static $boldFontStyle = ["bold" => true];
    public static $h1FontStyle = ["bold" => true, "size" => 16];
    public static $h2FontStyle = ["bold" => true, "size" => 14];

    public static $noSpaceParagraphStyle = ["spaceAfter" => 0];
    public static $centerParagraphStyle = ["align" => 'center'];
    public static $rightParagraphStyle = ["align" => 'right'];


    /**
     * Конвертирует массив даанных в таблицу Word
     * @param array $data Массив
     * @param \PhpOffice\PhpWord\Element\Section $section section, в которую будет добавляться таблица
     * @return \PhpOffice\PhpWord\Element\Table
     */
    public static function getTable(array $data, \PhpOffice\PhpWord\Element\Section $section)
    {
        $table = $section->addTable();
        foreach ($data as $row)
        {
            $table->addRow();
            foreach ($row as $column)
            {
                $table->addCell(1750)->addText($column);
            }
        }
        return $table;
    }

    public static function saveAsHtml(PhpWord $wordFile, $name)
    {
        $objWriter = IOFactory::createWriter($wordFile, 'HTML');
        $filename = $name;
        $objWriter->save($filename);

        $content = file_get_contents($filename);
  //      unlink($filename);
        return $content;
    }

    public static function saveAsWord(PhpWord $wordFile, $name)
    {
        $objWriter = IOFactory::createWriter($wordFile, 'Word2007');
        $filename = $name;
        $objWriter->save($filename);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($filename));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        flush();
        readfile($filename);
    }

    public static function saveAsPdf(PhpWord $wordFile, $name)
    {
//        require_once '/vendor/phpoffice/phpword/src/PhpWord/Autoloader.php';
     //   \PhpOffice\PhpWord\Autoloader::register();
        \PhpOffice\PhpWord\Settings::setPdfRendererPath(base_path('/vendor/dompdf/dompdf'));
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

        $objWriter = IOFactory::createWriter($wordFile, 'PDF');
        $filename = $name;
        $objWriter->save($filename);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($filename));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        flush();
        readfile($filename);
    }




}