<?php

namespace App\Repositories;

use App\Models\SearchRequest;
use InfyOm\Generator\Common\BaseRepository;

class SearchRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SearchRequest::class;
    }
}
