<?php
/**
 * Created by JetBrains PhpStorm.
 * User: БОСС
 * Date: 07.12.16
 * Time: 12:21
 * To change this template use File | Settings | File Templates.
 */

namespace App\Repositories;


use App\Helpers\MetricHelper;
use App\Models\ReportConfig;
use App\Models\ReportParts\Diagram;
use App\Models\ReportParts\DimensionDiagram;
use App\Models\ReportParts\FreeHtmlText;
use App\Models\ReportParts\Header;
use App\Models\ReportParts\RequestTable;
use App\Models\ReportParts\SourcesDiagram;
use App\Models\ReportParts\Table;
use App\Models\ReportParts\Test;
use App\Models\Site;
use App\Models\ReportPart;

class ReportPartRepository {

    private $_site;

    public function __construct(Site $site)
    {
        $this->_site = $site;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->_site;
    }

    private $_totalId = 0;

    public function getNextPartId()
    {
        return $this->_totalId++;
    }

    /**
     * @param ReportConfig $config
     * @return ReportPart[]
     */
    public function getAll(ReportConfig $config = null)
    {
        if ($config == null)
        {
            $config = new ReportConfig();
            $config->setPeriod("2016-11-01","2016-12-01");
        }

        $table = new RequestTable($this, $config);
        $result[] = $table;

        $diagram = new SourcesDiagram($this, $config);
        $result[] = $diagram;

        foreach (MetricHelper::getYandexMetricCodes() as $metricCode)
        {
            $diagram = new DimensionDiagram($this,$config);
            $diagram->setMetricCode($metricCode);
            $result[] = $diagram;
        }

        $freeText = new FreeHtmlText($this, $config);
        $result[] = $freeText;

        $header = new Header($this, $config);
        $result[] = $header;

        return $result;
    }

    public function getByParams(array $params, ReportConfig $config)
    {
        $element = null;
        if ($params["type"] == "dimension-diagram")
        {
            $element = new DimensionDiagram($this, $config);
            $element->setMetricCode($params["metric"]);
        }

        if ($params["type"] == "free-text")
        {
            $element = new FreeHtmlText($this, $config);
        }

        if ($params["type"] == "header")
        {
            $element = new Header($this, $config);
        }

        if ($params["type"] == "sources-diagram")
        {
            $element = new SourcesDiagram($this, $config);
        }

        if ($params["type"] == "request-table")
        {
            $element = new RequestTable($this, $config);
        }

        if ($element != null)
            $element->applyParams($params);
        return $element;

    }


}