<?php

namespace App\Repositories;

use App\Models\ReportConfig;
use App\Models\Site;
use App\Models\ReportHistory;
use InfyOm\Generator\Common\BaseRepository;
use Symfony\Component\HttpFoundation\Request;

class ReportHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportHistory::class;
    }

    public static function add(Request $request, Site $site, $type)
    {
        $config = new ReportConfig();
        $config->setFromRequest($request);
        $dates = array_map(function($elem) { return $elem->format("Y-m-d");}, $config->getPeriod());

        $history =  new ReportHistory();
        $history->dateStart = $dates[0];
        $history->dateEnd = $dates[1];
        $history->idSite = $site->id;
        // TODO:: change this property honestly
        $history->idTemplate = 1;
        $history->idUser = 1;
        $history->fileName = "??";
        $history->type = $type;
        $history->save(); // Сохраняем в первый раз, чтобы получить id в БД
        $history->fileName = $history->getPath();
        $history->save();
        return $history;
    }

    public static function get(array $params)
    {
        $history = ReportHistory::latest()
            ->limit(10);
        foreach ($params as $key => $value)
            $history->where($key, $value);

        return $history->get();
    }
}
