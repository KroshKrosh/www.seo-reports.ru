<?php

namespace App\Repositories;

use App\Models\Site;
use App\Models\Template;
use InfyOm\Generator\Common\BaseRepository;

class TemplateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Template::class;
    }


    public static function get()
    {
        return Template::all();
    }

}
